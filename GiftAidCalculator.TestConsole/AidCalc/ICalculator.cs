namespace GiftAidCalculator.TestConsole.AidCalc
{
    public interface ICalculator
    {
        decimal Calculate(decimal amount);
    }
}