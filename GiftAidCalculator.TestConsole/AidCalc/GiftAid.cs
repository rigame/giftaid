﻿using System;
using GiftAidCalculator.TestConsole.DataAccess;
using GiftAidCalculator.TestConsole.Rounders;
using GiftAidCalculator.TestConsole.Supplements;

namespace GiftAidCalculator.TestConsole.AidCalc
{
    public class GiftAid : ICalculator
    {
        private readonly decimal _giftAidRatio;
        private readonly IRounder _rounder;
        private readonly BaseSupplement _supplement;

        // Note Decided:
        // -DataStore is not optional -> refactor the code to have it as parameter.
        // -Rounding is optional -> created a contructor overload and IdentityRounder class.
        public GiftAid(IDataStore taxDataStore) : this(taxDataStore, new IdentityRounder())
        {
        }

        public GiftAid(IDataStore taxDataStore, IRounder rounder)
            : this(taxDataStore, rounder, new NoSupplement())
        {
        }

        public GiftAid(IDataStore taxDataStore, IRounder rounder, BaseSupplement supplement)
        {
            if (taxDataStore == null) throw new ArgumentNullException("taxDataStore");
            if (rounder == null) throw new ArgumentNullException("rounder");
            if (supplement == null) throw new ArgumentNullException("supplement");

            _rounder = rounder;
            _supplement = supplement;

            var taxRate = taxDataStore.GetTaxRate();
            if (taxRate < 0 || taxRate >= 100) throw new ArgumentOutOfRangeException();

            _giftAidRatio = taxRate / (100 - taxRate);
        }

        public decimal Calculate(decimal donationAmount)
        {
            return Calculate(donationAmount, string.Empty);

        }

        public decimal Calculate(decimal donationAmount, string eventType)
        {
            if (donationAmount < 0) throw new ArgumentOutOfRangeException("donationAmount", donationAmount, "Negative amounts not allowed.");

            return  _rounder.Round(_supplement.Calculate(donationAmount, eventType) * _giftAidRatio);
        }
    }
}
