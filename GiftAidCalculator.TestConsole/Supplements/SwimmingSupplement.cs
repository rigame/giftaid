﻿using System;

namespace GiftAidCalculator.TestConsole.Supplements
{
    public class SwimmingSupplement : BaseSupplement
    {
        public override decimal Calculate(decimal amount, string eventType)
        {
            if (string.Compare(eventType, EventType.Swimming.ToString(), StringComparison.CurrentCultureIgnoreCase) == 0)
                return amount + amount * 0.03m;

            return Successor.Calculate(amount, eventType);
        }
    }
}
