﻿namespace GiftAidCalculator.TestConsole.Supplements
{
    /// <summary>
    /// Story 4 doesn't ask to get values from DataStore as in Story 2 so
    /// interpret its required them to be hardcoded.
    /// In a real project, will get supplement values from DataStore
    /// as very likely event promoter will want to change/add these values.
    /// </summary>
    public abstract class BaseSupplement
    {
        protected BaseSupplement Successor;

        public void SetSuccesssor(BaseSupplement successor)
        {
            Successor = successor;
        }

        public abstract decimal Calculate(decimal amount, string eventType);
    }
}
