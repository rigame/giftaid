﻿using System;

namespace GiftAidCalculator.TestConsole.Supplements
{
    public class RunningSupplement : BaseSupplement
    {
        public override decimal Calculate(decimal amount, string eventType)
        {
            if (string.Compare(eventType, EventType.Running.ToString(), StringComparison.CurrentCultureIgnoreCase) == 0)
                return amount + amount * 0.05m;

            return Successor.Calculate(amount, eventType);
        }
    }
}
