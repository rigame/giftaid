﻿namespace GiftAidCalculator.TestConsole.Supplements
{
    public class NoSupplement : BaseSupplement
    {
        public override decimal Calculate(decimal amount, string eventType)
        {
            return amount;
        }
    }
}
