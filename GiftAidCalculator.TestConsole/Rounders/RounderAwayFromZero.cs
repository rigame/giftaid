﻿using System;

namespace GiftAidCalculator.TestConsole.Rounders
{
    public class RounderAwayFromZero : IRounder
    {
        public decimal Round(decimal value)
        {
            return decimal.Round(value, 2, MidpointRounding.AwayFromZero);
        }
    }
}
