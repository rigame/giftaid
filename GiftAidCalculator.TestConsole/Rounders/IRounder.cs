﻿namespace GiftAidCalculator.TestConsole.Rounders
{
    public interface IRounder
    {
        decimal Round(decimal value);
    }
}