﻿using System;

namespace GiftAidCalculator.TestConsole.DataAccess
{
    /*
     * Stub: 
     * Defining the stub here instead of doing it in the test project
     * as it is used in Program.cs
     * At it is not real code will not create a class to test in the test project.
    */
    public class DataStoreStub : IDataStore
    {
        public decimal GetTaxRate()
        {
            /*
             * In a real project, here should go the call to the database 
             * as very likely event promoter will want to change these values
             */
            return 20;
        }
    }
}
