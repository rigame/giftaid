﻿namespace GiftAidCalculator.TestConsole.DataAccess
{
    public interface IDataStore
    {
        decimal GetTaxRate();
    }
}