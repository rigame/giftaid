﻿using System;
using GiftAidCalculator.TestConsole.AidCalc;
using GiftAidCalculator.TestConsole.DataAccess;
using GiftAidCalculator.TestConsole.Rounders;
using GiftAidCalculator.TestConsole.Supplements;

namespace GiftAidCalculator.TestConsole
{
    class Program
    {
        private static GiftAid _giftAid;

        static void Main(string[] args)
        {
            SetUp();

            Console.WriteLine("Please Enter donation amount:");
		    string amount = Console.ReadLine();
            Console.WriteLine("Please Enter event type:");
            string supplement = Console.ReadLine();

		    try
		    {
                Console.WriteLine(_giftAid.Calculate(decimal.Parse(amount), supplement));
		    }
		    catch (Exception ex)
		    {
                Console.WriteLine("There was a problem calculating the gift aid.");
            }

		    Console.WriteLine("Press any key to exit.");
			Console.ReadLine();
		}

        private static void SetUp()
        {
            var noSupplement = new NoSupplement();

            var runningSupplement = new RunningSupplement();
            runningSupplement.SetSuccesssor(noSupplement);

            var swimmingSupplement = new SwimmingSupplement();
            swimmingSupplement.SetSuccesssor(runningSupplement);

            _giftAid = new GiftAid(new DataStoreStub(), new RounderAwayFromZero(), swimmingSupplement);
        }
    }
}
