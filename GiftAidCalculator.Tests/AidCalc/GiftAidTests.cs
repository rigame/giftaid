﻿using System;
using GiftAidCalculator.TestConsole.AidCalc;
using GiftAidCalculator.TestConsole.DataAccess;
using GiftAidCalculator.TestConsole.Rounders;
using GiftAidCalculator.TestConsole.Supplements;
using Moq;
using NUnit.Framework;

namespace GiftAidCalculator.Tests
{
    [TestFixture]
    public class GiftAidTests
    {
        private GiftAid _giftAid;

        [SetUp]
        public void SetUp()
        {
            var mockDataStore = new Mock<IDataStore>();
            mockDataStore.Setup(ds => ds.GetTaxRate()).Returns(20);
            _giftAid = new GiftAid(mockDataStore.Object);
        }

        [Test]
        public void GiftAid_DataStoreNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new GiftAid(null));
        }

        [Test]
        public void GiftAid_RounderNull_ThrowsArgumentNullException()
        {
            var mockDataStore = new Mock<IDataStore>();
            mockDataStore.Setup(ds => ds.GetTaxRate()).Returns(20);

            Assert.Throws<ArgumentNullException>(() => new GiftAid(mockDataStore.Object, null));
        }

        [Test]
        public void GiftAid_DataStoreGetsOutOfRangeValue_ThrowsArgumentOutOfRangeException()
        {
            var mockDataStore = new Mock<IDataStore>();
            mockDataStore.Setup(ds => ds.GetTaxRate()).Returns(120);
            Assert.Throws<ArgumentOutOfRangeException>(() => new GiftAid(mockDataStore.Object));
        }

        [Test]
        public void Calculate_AmountZero_ReturnsZero()
        {
            var result= _giftAid.Calculate(0);

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void Calculate_AmountNegative_ThrowsArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _giftAid.Calculate(-1));
        }

        [Test]
        public void Calculate_AmountMaxValue_ReturnsAidAmount()
        {
            var result = _giftAid.Calculate(Decimal.MaxValue);

            Assert.That(result, Is.EqualTo(Decimal.MaxValue * 0.25m));
        }

        [Test]
        public void Calculate_AmountPositive_ReturnsAidAmount()
        {
            var result = _giftAid.Calculate(10);

            Assert.That(result, Is.EqualTo(10 * 0.25m));
        }

        [Test]
        public void Calculate_AmountPositiveWithRounder_VerifyIsCalled()
        {
            var mockDataStore = new Mock<IDataStore>();
            mockDataStore.Setup(ds => ds.GetTaxRate()).Returns(20);

            var mockRounder = new Mock<IRounder>();

            var giftAid = new GiftAid(mockDataStore.Object, mockRounder.Object);

            giftAid.Calculate(10);

            mockRounder.Verify(r => r.Round(10 * 0.25m));
        }

        [Test]
        public void Calculate_AmountWithSupplement_VerifyIsCalled()
        {
            var mockDataStore = new Mock<IDataStore>();
            mockDataStore.Setup(ds => ds.GetTaxRate()).Returns(20);

            var mockRounder = new Mock<IRounder>();
            var mockSupplement = new Mock<BaseSupplement>();

            var giftAid = new GiftAid(mockDataStore.Object, mockRounder.Object, mockSupplement.Object);

            giftAid.Calculate(10, "test");

            mockSupplement.Verify(s => s.Calculate(10, "test"));
        }
    }
}
