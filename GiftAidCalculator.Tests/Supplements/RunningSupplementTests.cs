﻿using GiftAidCalculator.TestConsole.Supplements;
using Moq;
using NUnit.Framework;

namespace GiftAidCalculator.Tests.Supplements
{
    [TestFixture]
    public class RunningSupplementTests
    {
        private RunningSupplement _supplement;
        private Mock<BaseSupplement> _mockSupplement;

        [SetUp]
        public void SetUp()
        {
            _supplement = new RunningSupplement();

            _mockSupplement = new Mock<BaseSupplement>();
            _mockSupplement.Setup(s => s.Calculate(It.IsAny<decimal>(), It.IsAny<string>())).Returns(123);
            _supplement.SetSuccesssor(_mockSupplement.Object);
        }

        [Test]
        public void Calculate_RunningEvent_ReturnsAmountWith5Percent()
        {
            var result = _supplement.Calculate(10, EventType.Running.ToString());

            Assert.That(result, Is.EqualTo(10.5));
        }

        [Test]
        public void Calculate_OtherEvent_CallsSuccessor()
        {
            var result = _supplement.Calculate(10, "dancing");

            Assert.That(result, Is.EqualTo(123));
        }
    }
}
