﻿using GiftAidCalculator.TestConsole.Supplements;
using Moq;
using NUnit.Framework;

namespace GiftAidCalculator.Tests.Supplements
{
    [TestFixture]
    public class SwimmingSupplementTests
    {
        private SwimmingSupplement _supplement;
        private Mock<BaseSupplement> _mockSupplement;

        [SetUp]
        public void SetUp()
        {
            _supplement = new SwimmingSupplement();

            _mockSupplement = new Mock<BaseSupplement>();
            _mockSupplement.Setup(s => s.Calculate(It.IsAny<decimal>(), It.IsAny<string>())).Returns(123);

            _supplement.SetSuccesssor(_mockSupplement.Object);
        }

        [Test]
        public void Calculate_SwimmingEvent_ReturnsAmountWith3Percent()
        {
            var result = _supplement.Calculate(10, EventType.Swimming.ToString());

            Assert.That(result, Is.EqualTo(10.3));
        }

        [Test]
        public void Calculate_OtherEvent_CallsSuccessor()
        {
            var result = _supplement.Calculate(10, "dancing");

            Assert.That(result, Is.EqualTo(123));
        }
    }
}
