﻿using GiftAidCalculator.TestConsole;
using GiftAidCalculator.TestConsole.Rounders;
using NUnit.Framework;

namespace GiftAidCalculator.Tests
{
    [TestFixture]
    public class RounderTests
    {
        private RounderAwayFromZero _rounderAwayFromZero;

        [SetUp]
        public void SetUp()
        {
            _rounderAwayFromZero = new RounderAwayFromZero();
        }

        [Test]
        public void Round_ValueWith0Decimals_ReturnsValue()
        {
            var result= _rounderAwayFromZero.Round(1);

            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public void Round_ValueWith3DecimalBelow5_ReturnsValueRoundDown()
        {
            var result = _rounderAwayFromZero.Round(1.1149m);

            Assert.That(result, Is.EqualTo(1.11));
        }

        [Test]
        public void Round_ValueWith3Decimal5_ReturnsValueRoundUp()
        {
            var result = _rounderAwayFromZero.Round(2.625m);

            Assert.That(result, Is.EqualTo(2.63m));
        }
    }
}
